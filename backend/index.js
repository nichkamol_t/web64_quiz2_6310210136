const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET



const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root1',
    password : 'root1',
    database : 'BizMallSystem'
})

connection.connect();
const express = require('express')
const req = require('express/lib/request');
const res = require('express/lib/response');
const app = express()
const port = 4000


/* Middleware for Authenticating User Token */
function authenticateToken(req , res , next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET , (err , user) => {
        if (err) {return res.sendStatus(403)}
        else {
            req.user = user
            next()
        }
    })
}



/* API for Processing Seller Authorization */
app.post("/login", (req , res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Seller WHERE Username='${username}' `
    connection.query( query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from seller db"
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password , db_password, (err , result) => {
                if (result) {
                   let payload = {
                       "username" : rows[0].Username,
                       "user_id" : rows[0].RunnerID,
                       "IsAdmin" : rows[0].IsAdmin,
                   }
                   console.log(payload)
                   let token = jwt.sign(payload, TOKEN_SECRET , {expiresIn : '1d'})
                   res.send(token)
                }else{ res.send("Invalid username / password") }      
            })
        }
    })
})


/* API for Registering a new Seller */
app.post("/register_seller" , (req , res) => {

    let seller_name = req.query.seller_name
    let seller_surname = req.query.seller_surname
    let seller_username = req.query.seller_username
    let seller_password = req.query.seller_password
    
    bcrypt.hash(seller_password, SALT_ROUNDS, (err , hash) => {
        let query = `INSERT INTO Seller
                (SellerName , SellerSurname, Username , Password , IsAdmin) 
                VALUES ('${seller_name}' ,'${seller_surname}',
                        '${seller_username}',  '${hash}' , false)`
        console.log(query)
        connection.query(query, (err , rows) => {
        if (err) {
        res.json({ 
                "status" : "400",
                "message" : "Error inserting data into db"
                })
        }else {
        res.json({ 
                "status" : "200",
                "message" : "Adding new user succesful"
                })
                }
        });
    })
});


app.post("/list_seller" , authenticateToken , (req, res) => {

    let seller_id = req.query.seller_id

    if (!req.user.IsAdmin)  { res.send("Unauthorized Because you're not admin") }
    else {
    let query = `
    SELECT Seller.SellerID , Seller.SellerName , Seller.SellerSurname ,  
           Registration.StoreName , Registration.RegistrationTime
           FROM Seller , Registration , SalesArea

           WHERE (Registration.SellerID = Seller.SellerID) AND 
                 (Registration.AreaID = SalesArea.AreaID) AND
                 (Seller.SellerID = '${seller_id}');`

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from seller db"
                    })
        }else {
            res.json(rows)
        }
    });
  }
})

app.post("/register_Sales", authenticateToken ,  (req , res) => {

    let user_profile = req.user
    let seller_id = req.query.seller_id
    let area_id = req.query.area_id
    let storename = req.query.storename

    let query = `INSERT INTO Registration
                    (SellerID , AreaID , StoreName , RegistrationTime) 
                    VALUES ('${seller_id}' ,
                            '${area_id}' ,
                             '${storename}' ,
                             NOW() )`
    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Adding sales succesful"
                    })
        }
    });
});




app.get("/list_registration" , (req, res) => {
    let query = `
    SELECT Seller.SellerID , Seller.SellerName , Seller.SellerSurname , 
           SalesArea.AreaName , 
           Registration.StoreName, Registration.RegistrationTime 

           FROM Seller , Registration , SalesArea

           WHERE (Registration.SellerID = Seller.SellerID) AND 
           
           (Registration.AreaID = SalesArea.AreaID);`;

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from seller db"
                    })
        }else {
            res.json(rows)
        }
    });

})

/* CRUD Operation fot SalesArea Table */
app.post("/add_area", (req, res) => {
    let area_name = req.query.area_name
    let area_rentalfee = req.query.area_rentalfee

    let query = `INSERT INTO SalesArea 
                    (AreaName, RentalFee) 
                    VALUES ('${area_name}' ,'${area_rentalfee}' )`
    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Adding area succesful"
                    })
        }
    });


});


app.get("/list_area" , (req, res) => {
    let query = "SELECT * FROM SalesArea";
    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from area db"
                    })
        }else {
            res.json(rows)
        }
    });

})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})

app.post("/update_area", (req, res) => {

    let area_id = req.query.area_id
    let area_name = req.query.area_name
    let area_rentalfee = req.query.area_rentalfee

    let query = `UPDATE SalesArea SET 
                    AreaName='${area_name}',
                    RentalFee=${area_rentalfee}
                    WHERE AreaID=${area_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error updating record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Updating area successful"
                    })
        }
    });
})


app.post("/delete_area", (req, res) => {

    let area_id = req.query.area_id

    let query = `DELETE FROM SalesArea WHERE AreaID=${area_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error deleting record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "deleting record successful"
                    })
        }
    });
})

/*
query = "SELECT * from Seller";
connection.query(query, (err , rows) => {
        if (err) {
            console.log(err);
        }else {
            console.log(rows);
        }
});

connection.end(); */